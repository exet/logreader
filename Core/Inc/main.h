/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f0xx_ll_adc.h"
#include "stm32f0xx_ll_crs.h"
#include "stm32f0xx_ll_rcc.h"
#include "stm32f0xx_ll_bus.h"
#include "stm32f0xx_ll_system.h"
#include "stm32f0xx_ll_exti.h"
#include "stm32f0xx_ll_cortex.h"
#include "stm32f0xx_ll_utils.h"
#include "stm32f0xx_ll_pwr.h"
#include "stm32f0xx_ll_dma.h"
#include "stm32f0xx_ll_spi.h"
#include "stm32f0xx_ll_tim.h"
#include "stm32f0xx_ll_usart.h"
#include "stm32f0xx_ll_gpio.h"

#if defined(USE_FULL_ASSERT)
#include "stm32_assert.h"
#endif /* USE_FULL_ASSERT */

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define LCD_Light_Pin LL_GPIO_PIN_0
#define LCD_Light_GPIO_Port GPIOF
#define Charge_SD_Pin LL_GPIO_PIN_1
#define Charge_SD_GPIO_Port GPIOF
#define Ubat_Pin LL_GPIO_PIN_2
#define Ubat_GPIO_Port GPIOA
#define Buzzer_Pin LL_GPIO_PIN_3
#define Buzzer_GPIO_Port GPIOA
#define LCD_Reset_Pin LL_GPIO_PIN_4
#define LCD_Reset_GPIO_Port GPIOA
#define LCD_DC_Pin LL_GPIO_PIN_0
#define LCD_DC_GPIO_Port GPIOB
#define LCD_CS_Pin LL_GPIO_PIN_1
#define LCD_CS_GPIO_Port GPIOB
#define USB_Disc_Pin LL_GPIO_PIN_15
#define USB_Disc_GPIO_Port GPIOA
#define But1_Pin LL_GPIO_PIN_3
#define But1_GPIO_Port GPIOB
#define But1_EXTI_IRQn EXTI2_3_IRQn
#define But2_Pin LL_GPIO_PIN_4
#define But2_GPIO_Port GPIOB
#define But2_EXTI_IRQn EXTI4_15_IRQn
#define But3_Pin LL_GPIO_PIN_5
#define But3_GPIO_Port GPIOB
#define But3_EXTI_IRQn EXTI4_15_IRQn
#ifndef NVIC_PRIORITYGROUP_0
#define NVIC_PRIORITYGROUP_0         ((uint32_t)0x00000007) /*!< 0 bit  for pre-emption priority,
                                                                 4 bits for subpriority */
#define NVIC_PRIORITYGROUP_1         ((uint32_t)0x00000006) /*!< 1 bit  for pre-emption priority,
                                                                 3 bits for subpriority */
#define NVIC_PRIORITYGROUP_2         ((uint32_t)0x00000005) /*!< 2 bits for pre-emption priority,
                                                                 2 bits for subpriority */
#define NVIC_PRIORITYGROUP_3         ((uint32_t)0x00000004) /*!< 3 bits for pre-emption priority,
                                                                 1 bit  for subpriority */
#define NVIC_PRIORITYGROUP_4         ((uint32_t)0x00000003) /*!< 4 bits for pre-emption priority,
                                                                 0 bit  for subpriority */
#endif
/* USER CODE BEGIN Private defines */

#define SIZE_POINT_SNAKE		4		// Размер квадратика змейки
#define SIZE_LINE				320
#define SIZE_COLUMN	  			240
#define SIZE_LINE_FOR_SNAKE		240		// размер активной области экрана для змейки по высоте
#define SIZE_COLUMN_FOR_SNAKE	240		// размер активной области экрана для змейки по ширине


#define NUMBER_POINT_LINE		(SIZE_LINE_FOR_SNAKE/SIZE_POINT_SNAKE)	// количество доступных позиций по высоте
#define NUMBER_POINT_COLUMN		(SIZE_COLUMN_FOR_SNAKE/SIZE_POINT_SNAKE)	// количество доступных позиций по ширине

#define SIZE_MATRIX				(NUMBER_POINT_LINE * NUMBER_POINT_COLUMN) // количество элементов в матрице (позиций для змейки)

#define MAX_LENGHT_SNAKE		(SIZE_MATRIX / 4)						// максимальный размер змейки

#define	DIR_LEFT				(-NUMBER_POINT_COLUMN)
#define	DIR_RIGHT				NUMBER_POINT_COLUMN
#define	DIR_DOWN				1
#define	DIR_UP					-1

#define	GO_STRAIGHT				0
#define	GO_RIGHT				1
#define	GO_LEFT					2
#define	GO_DOWN					3
#define	GO_UP					4


#define COMAND 	      	LL_GPIO_ResetOutputPin(LCD_DC_GPIO_Port, LCD_DC_Pin);
#define DATA 		   	LL_GPIO_SetOutputPin(LCD_DC_GPIO_Port, LCD_DC_Pin);

#define RES_ON 		    LL_GPIO_ResetOutputPin(LCD_Reset_GPIO_Port, LCD_Reset_Pin);
#define RES_OFF 	    LL_GPIO_SetOutputPin(LCD_Reset_GPIO_Port, LCD_Reset_Pin);

#define CS_ON 	    	LL_GPIO_ResetOutputPin(LCD_CS_GPIO_Port, LCD_CS_Pin); 
#define CS_OFF 		    LL_GPIO_SetOutputPin(LCD_CS_GPIO_Port, LCD_CS_Pin); 

#define MS 	      		6000
#define uS 	      		6

#define SIZE_DISPLAY  76800


#define NUMBER_OF_LINES	  18				// Количество текстовых строк выводимых на дисплей


extern void Delay(uint32_t);

#define CODE_ENTER		0x0D
#define CODE_END_LINE	0x0A

#define ALL_STRING 		1
#define SECTOR 			2

#define N_SYS_TIMERS	4

void Print_Data_to_display(uint8_t , uint16_t ,char* );


/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
