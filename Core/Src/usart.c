/**
  ******************************************************************************
  * File Name          : USART.c
  * Description        : This file provides code for the configuration
  *                      of the USART instances.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "usart.h"

/* USER CODE BEGIN 0 */

extern uint8_t flag_usart;
// USART Receiver buffer
extern volatile uint8_t rx_buffer[];
extern volatile uint16_t rx_wr_index, rx_rd_index;
extern volatile uint16_t rx_counter;
extern volatile uint8_t rx_buffer_overflow;

// Transmitter buffer
extern volatile uint8_t tx_buffer[];
extern volatile uint16_t tx_wr_index, tx_rd_index;
extern volatile uint16_t tx_counter;

void
SPI_WriteComm ();
void
SPI_WriteData ();

void
USART_init (void);
uint8_t
get_char (void);
void
put_char (uint8_t);
void
put_str (unsigned char*);
void
put_int (int32_t data);

void
USART2_IRQHandler (void)
{

  /*
   if(USART_GetITStatus(USART2, USART_IT_RXNE) == SET)
   {
   if ((USART2->SR & (USART_FLAG_NE|USART_FLAG_FE|USART_FLAG_PE|USART_FLAG_ORE)) == 0)
   {
   rx_buffer[rx_wr_index]=(uint8_t)(USART_ReceiveData(USART2)& 0xFF);

   rx_wr_index++;
   if (rx_wr_index == RX_BUFFER_SIZE)
   rx_wr_index=0;

   }
   else USART_ReceiveData(USART2);																		// вообще здесь нужен обработчик ошибок, а мы просто пропускаем битый байт
   }

   if(USART_GetITStatus(USART2, USART_IT_ORE_RX) == SET) 							// прерывание по переполнению буфера
   {
   USART_ReceiveData(USART2); 																				// в идеале пишем здесь обработчик переполнения буфера, но мы просто сбрасываем этот флаг прерывания чтением из регистра данных.
   }

   if(USART_GetITStatus(USART2, USART_IT_TXE) == SET)
   {
   if (tx_counter)
   {
   --tx_counter;
   USART_SendData(USART2,tx_buffer[tx_rd_index++]);
   if (tx_rd_index == TX_BUFFER_SIZE) tx_rd_index=0;
   }
   else
   {
   USART_ITConfig(USART2, USART_IT_TXE, DISABLE);
   }
   }
   */
}

uint8_t
get_char (void)
{
  /*
   uint8_t data;
   while (rx_counter==0);
   data=rx_buffer[rx_rd_index++];
   if (rx_rd_index == RX_BUFFER_SIZE)
   {
   rx_rd_index=0;
   flag_usart = 1;
   }
   USART_ITConfig(USART2, USART_IT_RXNE, DISABLE);
   --rx_counter;
   USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);
   return data;
   */
}

void
put_char (uint8_t c)
{
  /*
   while (tx_counter == TX_BUFFER_SIZE);
   USART_ITConfig(USART2, USART_IT_TXE, DISABLE);
   if (tx_counter || (USART_GetFlagStatus(USART2, USART_FLAG_TXE) == RESET))
   {
   tx_buffer[tx_wr_index++]=c;
   if (tx_wr_index == TX_BUFFER_SIZE) tx_wr_index=0;
   ++tx_counter;
   USART_ITConfig(USART2, USART_IT_TXE, ENABLE);
   }
   else
   */
  while (!(USART1->ISR & USART_ISR_TC))
    ;
  LL_USART_TransmitData8 (USART1, c);
}

void put_str (unsigned char *s)
{
  while (*s != 0)
    put_char (*s++);
}

void
put_int (int32_t data)
{
  unsigned char temp[10], count = 0;
  if (data < 0)
    {
      data = -data;
      put_char ('-');
    }

  if (data)
    {
      while (data)
	{
	  temp[count++] = data % 10 + '0';
	  data /= 10;
	}

      while (count)
	{
	  put_char (temp[--count]);
	}
    }
  else
    put_char ('0');

}

/* USER CODE END 0 */

/* USART1 init function */

void MX_USART1_UART_Init(void)
{
  LL_USART_InitTypeDef USART_InitStruct = {0};

  LL_GPIO_InitTypeDef GPIO_InitStruct = {0};
  /* Peripheral clock enable */
  LL_APB1_GRP2_EnableClock(LL_APB1_GRP2_PERIPH_USART1);
  
  LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOB);
  /**USART1 GPIO Configuration  
  PB6   ------> USART1_TX
  PB7   ------> USART1_RX 
  */
  GPIO_InitStruct.Pin = LL_GPIO_PIN_6;
  GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
  GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_HIGH;
  GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
  GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
  GPIO_InitStruct.Alternate = LL_GPIO_AF_0;
  LL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  GPIO_InitStruct.Pin = LL_GPIO_PIN_7;
  GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
  GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_HIGH;
  GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
  GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
  GPIO_InitStruct.Alternate = LL_GPIO_AF_0;
  LL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /* USART1 interrupt Init */
  NVIC_SetPriority(USART1_IRQn, 0);
  NVIC_EnableIRQ(USART1_IRQn);

  USART_InitStruct.BaudRate = 57600;
  USART_InitStruct.DataWidth = LL_USART_DATAWIDTH_8B;
  USART_InitStruct.StopBits = LL_USART_STOPBITS_1;
  USART_InitStruct.Parity = LL_USART_PARITY_NONE;
  USART_InitStruct.TransferDirection = LL_USART_DIRECTION_TX_RX;
  USART_InitStruct.HardwareFlowControl = LL_USART_HWCONTROL_NONE;
  USART_InitStruct.OverSampling = LL_USART_OVERSAMPLING_16;
  LL_USART_Init(USART1, &USART_InitStruct);
  LL_USART_DisableIT_CTS(USART1);
  LL_USART_ConfigAsyncMode(USART1);
  LL_USART_Enable(USART1);

}

/* USER CODE BEGIN 1 */

/* USER CODE END 1 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
