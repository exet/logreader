/*
 * st7789v.c
 *
 *  Created on: 7 мар. 2020 г.
 *      Author: exet
 */

#include "st7789v.h"

void ST7789V_initial(void)
{
  //-----------------------------------ST7789V reset sequence------------------------------------//
  RES_OFF;
  Delay(1*MS); 					//Delay 1ms
  RES_ON;
  Delay(10*MS); 				//Delay 10ms
  RES_OFF;
  Delay(120*MS); 				//Delay 120ms
  //---------------------------------------------------------------------------------------------------//
  SPI_WriteComm(0x11);
  Delay(120*MS); 				//Delay 120ms

  //-------------------------------display and color format setting-----------------------------//
  SPI_WriteComm(0x36);
//  SPI_WriteData(0x00);
  SPI_WriteData(0xA0);

  SPI_WriteComm(0x21);

  SPI_WriteComm(0x3A);
  SPI_WriteData(0x06);
  //--------------------------------ST7789V Frame rate

  //----------------------------------//
  SPI_WriteComm(0xB2);
  SPI_WriteData(0x05);
  SPI_WriteData(0x05);
  SPI_WriteData(0x00);
  SPI_WriteData(0x33);
  SPI_WriteData(0x33);
  SPI_WriteComm(0xB7);
  SPI_WriteData(0x35); //VGH=13V, VGL=-10.4V
  //------------------------------------------------------------------------------

  SPI_WriteComm(0xB7);
  SPI_WriteData(0x71);

  SPI_WriteComm(0xBB);
  SPI_WriteData(0x1E);

  SPI_WriteComm(0xC0);
  SPI_WriteData(0x2C);

  SPI_WriteComm(0xC2);
  SPI_WriteData(0x01);

  SPI_WriteComm(0xC3);
  SPI_WriteData(0x17);

  SPI_WriteComm(0xC4);
  SPI_WriteData(0x20);

  SPI_WriteComm(0xC6);
  SPI_WriteData(0x0F);

  SPI_WriteComm(0xD0);
  SPI_WriteData(0xA4);
  SPI_WriteData(0xA1);

  SPI_WriteComm(0xD6);
  SPI_WriteData(0xA1);
  //----------------------//   //gamma setting
  SPI_WriteComm(0xE0);
  SPI_WriteData(0xD0);
  SPI_WriteData(0x09);
  SPI_WriteData(0x14);
  SPI_WriteData(0x00);
  SPI_WriteData(0x00);
  SPI_WriteData(0x00);
  SPI_WriteData(0x31);
  SPI_WriteData(0x66);
  SPI_WriteData(0x46);
  SPI_WriteData(0x09);
  SPI_WriteData(0x14);
  SPI_WriteData(0x13);
  SPI_WriteData(0x29);
  SPI_WriteData(0x2D);

  SPI_WriteComm(0xE1);
  SPI_WriteData(0xD0);
  SPI_WriteData(0x09);
  SPI_WriteData(0x0C);
  SPI_WriteData(0x00);
  SPI_WriteData(0x00);
  SPI_WriteData(0x39);
  SPI_WriteData(0x2E);
  SPI_WriteData(0x34);
  SPI_WriteData(0x47);
  SPI_WriteData(0x36);
  SPI_WriteData(0x11);
  SPI_WriteData(0x11);
  SPI_WriteData(0x27);
  SPI_WriteData(0x2F);

  SPI_WriteComm(0x11);

  Delay(120*MS);
  SPI_WriteComm(0x29); //display on
}

