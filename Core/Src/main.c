/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "spi.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stdlib.h"
#include "st7789v.h"
#include "string.h"
#include "Consolas9x16.h"
#include "stm32f0xx_it.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */



/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
extern void ST7789V_initial(void);

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
extern const unsigned short Consolas9x16[];
uint8_t But1 = 0;
uint8_t But2 = 0;
uint8_t But3 = 0;
volatile uint16_t buff[240] = {0};
uint8_t symbol = ' ';
uint16_t i = 0;
uint16_t j = 0;
uint16_t n = 0;
uint16_t vsa = 0;
uint8_t flag_clear_line = 0;
uint8_t cnt_clear_line = 0;
uint8_t cnt_line_write = 0;

uint16_t data_ADC1 = 0;
uint32_t data_voltege_1 = 0;

uint8_t red;
uint8_t green;
uint8_t blue;

uint16_t X;
uint16_t Y;

uint32_t tmp_rand = 0;
uint16_t adc_tmp_rand = 0;
uint8_t generate_random = 0;


typedef struct
{
	uint8_t  red;
	uint8_t  green;
	uint8_t  blue;
	uint16_t coordinate;
	uint16_t time;
	uint8_t  size;

} apple_point_t;

apple_point_t apple_point;

typedef struct
{
	uint8_t  red;
	uint8_t  green;
	uint8_t  blue;
	uint16_t index_body;
	uint16_t index_hand;
	uint8_t  hand_red;
	uint8_t  hand_green;
	uint8_t  hand_blue;
	uint16_t index_tail;
	uint16_t length;
	int8_t  direction;
	volatile uint16_t coordinate_snake[MAX_LENGHT_SNAKE];
} Snake_point_t;

Snake_point_t snake;

//struct snake_2
//{
//	uint8_t  red;
//	uint8_t  green;
//	uint8_t  blue;
//	uint16_t coordinate_hand;
//	uint16_t length;
//	uint8_t  direction;
//	volatile uint16_t coordinate_snake[MAX_LENGHT_SNAKE];
//};

//volatile uint8_t matrix_snake[(SIZE_MATRIX/8)+1] = {0};

//char data_test[] = "АБВГДЕЁЖЗ�?ЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдеёжзийклмнопрстуфхцчшщъыьэюя";
//char data_test[] = "АаБбВвГгДдЕеЁёЖжЗз�?иЙйКк";
//char data_test[] = "ЛлМмНнОоПпРрСсТтУуФфХхЦц";
char data_test[] = "ЧчШшЩщЪъЫыЬьЭэЮюЯя";
const char table_SYM[] = "ЁёАБВГДЕЖЗ�?ЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдежзийклмнопрстуфхцчшщъыьэюя";

uint16_t sys_time_ms = 0;
uint8_t  sys_time_s  = 0;
uint8_t  sys_time_m  = 0;
uint8_t  sys_time_h  = 0;
uint16_t sys_time_d  = 0;

uint32_t sys_timers[N_SYS_TIMERS] = {0};

uint8_t Data_to_display[24] = {0};

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

void Delay(uint32_t cnt_delay)
{
  while(cnt_delay--)
  {
  };
}

void Convert_String(char* str)
{
  uint8_t indx=0;
  uint8_t indx2=0;
  uint8_t flag=0;
	
  while(str[indx])
  {
    if(str[indx] == 208)
    {
      flag=1;
      if(str[indx+1] == 129) str[indx] = str[indx+1] - 1;  	// для буквы Ё
      else str[indx] = str[indx+1] - 14;										// для остабльных рус. букв со старшим байтом 208
    }
    if(str[indx] == 209)
    {
      flag=1;
      if(str[indx+1] == 145) str[indx] = str[indx+1] - 16;	// для буквы ё
      else str[indx] = str[indx+1] + 50;										// для остабльных рус. букв со старшим байтом 209
    }
    if(flag)
    {
      indx2 = indx+1;
      while(str[indx2])
      {
	str[indx2] = str[indx2+1];
	indx2 ++;
      }
      flag = 0;
    }
    indx ++;
  }
}



void Print_String(char* str)
{
  Print_Data_to_display(ALL_STRING,240,str);
}

void Print_Sys_time(void)
{
  char send_text[9] = {0};
  send_text[0] = 0x30+(sys_time_m/10);
  send_text[1] = 0x30+(sys_time_m%10);
  send_text[2] = ':';
  send_text[3] = 0x30+(sys_time_s/10);
  send_text[4] = 0x30+(sys_time_s%10);
  send_text[5] = ':';
  send_text[6] = 0x30+(sys_time_ms/100);
  send_text[7] = 0x30+((sys_time_ms/10)%10);

  Print_Data_to_display(SECTOR,80,send_text);
}

void Print_Data_to_display(uint8_t mode, uint16_t cnt_px,char* str)
{
  uint8_t indx = 0;
  char buff_text[256] = {0};
  while(str[indx])
  {
    buff_text[indx] = str[indx];
    indx ++;
  }
  Convert_String(buff_text);
  while(buff_text[indx])
    indx++;
  indx=0;
  while(buff_text[indx]&&(indx<24))
  {
    for(i = 0; i < 9; i++)
    {
      buff[i+indx*10] = Consolas9x16[(((buff_text[indx]-32)*19)+(i*2)+1)+1];
      buff[i+indx*10] = buff[i+indx*10]<<8;
      buff[i+indx*10] += Consolas9x16[(((buff_text[indx]-32)*19)+(i*2))+1];
    }
    buff[i+indx*10] = 0;
    indx++;
  }
	
  switch(mode)
  {
    case ALL_STRING: 	SPI_WriteComm(0x3C);break;
    case SECTOR: 			SPI_WriteComm(0x2C);break;
  }
  CS_ON;
  DATA;

  for(i = 0; i < 16; i++)
  {
    for(j = 0; j < indx*10; j++)
    {
      if((buff[j])&(1<<i))
	print_pix_color(0, 0, 0);
      else
	print_pix_color(255, 255, 255);
    }
    for(j=j;j < cnt_px; j++)
      print_pix_color(255, 255, 255);
  }

  Delay(3);
  while(SPI1->SR & SPI_SR_BSY);
  CS_OFF;
	
}

void print_snake_point(Snake_point_t point)
{
	X = point.coordinate_snake[snake.index_body] / (NUMBER_POINT_COLUMN);
	Y = point.coordinate_snake[snake.index_body] % (NUMBER_POINT_COLUMN);

	SetXY_windows ((X*SIZE_POINT_SNAKE), ((X+1)*SIZE_POINT_SNAKE)-1, (Y*SIZE_POINT_SNAKE), ((Y+1)*SIZE_POINT_SNAKE)-1);

	SPI_WriteComm(0x2C);
	CS_ON;
	DATA;
	for(uint8_t i=0; i<(SIZE_POINT_SNAKE*SIZE_POINT_SNAKE); i++)
	{
		print_pix_color(point.red, point.green, point.blue);
		Delay(3);
		while(SPI1->SR & SPI_SR_BSY);
	}

	CS_OFF;

	X = point.coordinate_snake[point.index_hand] / (NUMBER_POINT_COLUMN);
	Y = point.coordinate_snake[point.index_hand] % (NUMBER_POINT_COLUMN);

	SetXY_windows ((X*SIZE_POINT_SNAKE), ((X+1)*SIZE_POINT_SNAKE)-1, (Y*SIZE_POINT_SNAKE), ((Y+1)*SIZE_POINT_SNAKE)-1);

	SPI_WriteComm(0x2C);
	CS_ON;
	DATA;
	for(uint8_t i=0; i<(SIZE_POINT_SNAKE*SIZE_POINT_SNAKE); i++)
	{
		print_pix_color(point.hand_red, point.hand_green, point.hand_blue);
		Delay(3);
		while(SPI1->SR & SPI_SR_BSY);
	}

	CS_OFF;


}

void clear_snake_point(uint16_t point_clear)
{
	X = snake.coordinate_snake[point_clear] / (NUMBER_POINT_COLUMN);
	Y = snake.coordinate_snake[point_clear] % (NUMBER_POINT_COLUMN);

	SetXY_windows ((X*SIZE_POINT_SNAKE), ((X+1)*SIZE_POINT_SNAKE)-1, (Y*SIZE_POINT_SNAKE), ((Y+1)*SIZE_POINT_SNAKE)-1);

	SPI_WriteComm(0x2C);
	CS_ON;
	DATA;
	for(uint8_t i=0; i<(SIZE_POINT_SNAKE*SIZE_POINT_SNAKE); i++)
	{
		print_pix_color(255, 255, 255);
		Delay(3);
		while(SPI1->SR & SPI_SR_BSY);
	}
	CS_OFF;
}

void generate_apple(void)
{
	tmp_rand = 0;
	generate_random = 0;
	uint8_t n_cycle = 0;

	while(generate_random == 0)
	{
		tmp_rand += adc_tmp_rand + n_cycle;
		tmp_rand = ((tmp_rand)*(SIZE_MATRIX - 1))>>16 ;
		tmp_rand %= SIZE_MATRIX;
		generate_random = 1;
		for(uint16_t i = 0; i < snake.length; i++)
		{
			if((snake.coordinate_snake[(snake.index_tail + i)%MAX_LENGHT_SNAKE]) == tmp_rand)
				generate_random = 0;
		}
		n_cycle ++;
	}

	apple_point.coordinate = tmp_rand;

	X = apple_point.coordinate / (NUMBER_POINT_COLUMN);
	Y = apple_point.coordinate % (NUMBER_POINT_COLUMN);

	SetXY_windows ((X*SIZE_POINT_SNAKE), ((X+1)*SIZE_POINT_SNAKE)-1, (Y*SIZE_POINT_SNAKE), ((Y+1)*SIZE_POINT_SNAKE)-1);

	SPI_WriteComm(0x2C);
	CS_ON;
	DATA;
	for(uint8_t i=0; i<(SIZE_POINT_SNAKE*SIZE_POINT_SNAKE); i++)
	{
		print_pix_color(apple_point.red, apple_point.green, apple_point.blue);
		Delay(3);
		while(SPI1->SR & SPI_SR_BSY);
	}

	CS_OFF;
}

void snake_brange(void)
{
	uint8_t solve_X = GO_STRAIGHT;
	uint8_t solve_Y = GO_STRAIGHT;
	uint16_t X_apple = apple_point.coordinate / (NUMBER_POINT_COLUMN);
	uint16_t Y_apple = apple_point.coordinate % (NUMBER_POINT_COLUMN);
	uint16_t X_hand  = snake.coordinate_snake[snake.index_hand] / (NUMBER_POINT_COLUMN);
	uint16_t Y_hand  = snake.coordinate_snake[snake.index_hand] % (NUMBER_POINT_COLUMN);
	if(X_apple>X_hand) solve_X = GO_RIGHT;
	if(X_apple<X_hand) solve_X = GO_LEFT;
	if(Y_apple>Y_hand) solve_Y = GO_DOWN;
	if(Y_apple<Y_hand) solve_Y = GO_UP;

	switch(snake.direction)
	{
	case DIR_LEFT:
		{
			switch(solve_Y)
			{
			case GO_STRAIGHT:	Button[1] = 1;	break;
			case GO_DOWN: 	Button[0] = 1;	break;
			case GO_UP:		Button[2] = 1;	break;
			}
		};	break;
	case DIR_RIGHT:
		{
			switch(solve_Y)
			{
			case GO_STRAIGHT:	Button[1] = 1;	break;
			case GO_DOWN: 	Button[2] = 1;	break;
			case GO_UP:		Button[0] = 1;	break;
			}
		};	break;
	case DIR_DOWN:
		{
			switch(solve_X)
			{
			case GO_STRAIGHT:	Button[1] = 1;	break;
			case GO_RIGHT: 	Button[0] = 1;	break;
			case GO_LEFT:	Button[2] = 1;	break;
			}
		};	break;
	case DIR_UP:
		{
			switch(solve_X)
			{
			case GO_STRAIGHT:	Button[1] = 1;	break;
			case GO_RIGHT: 	Button[2] = 1;	break;
			case GO_LEFT:	Button[0] = 1;	break;
			}
		};	break;
	}

}

void check_rules(void)
{
	uint16_t i = 0;
	uint8_t flag_eat_body = 0;
	while(flag_eat_body == 0)
	{
		if(snake.coordinate_snake[snake.index_hand] == snake.coordinate_snake[(i+snake.index_tail + MAX_LENGHT_SNAKE)%MAX_LENGHT_SNAKE])
			flag_eat_body = 1;
		else
		{
			if(i<snake.length - 1)
				i++;
			else
				flag_eat_body = 2;
		}
	}
	if(flag_eat_body == 1)
	{
		for(uint16_t j = 0; j<i; j++)
		{
			clear_snake_point((j+snake.index_tail + MAX_LENGHT_SNAKE)%MAX_LENGHT_SNAKE);
		}
		snake.length -= (i+1);
		snake.index_tail += i+1;
		if(snake.index_tail >= MAX_LENGHT_SNAKE)
			snake.index_tail %= MAX_LENGHT_SNAKE;
	}
}

void Print_Score (void)
{
	char send_text[9] = {0};
	send_text[0] = 147;
	send_text[1] = 185;
	send_text[2] = 129;
	send_text[3] = 180;
	send_text[4] = ':';

	send_text[5] = 0x30+(snake.length/100);
	send_text[6] = 0x30+((snake.length/10)%10);
	send_text[7] = 0x30+((snake.length)%10);

	SetXY_windows (240, 319, 22, 37);

	Print_Data_to_display(SECTOR,80,send_text);

}


/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
  char tmpdr[24] = { 0 };

  /* USER CODE END 1 */
  

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  

  LL_APB1_GRP2_EnableClock(LL_APB1_GRP2_PERIPH_SYSCFG);
  LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_PWR);

  /* System interrupt init*/

  LL_SYSCFG_EnablePinRemap();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_ADC_Init();
  MX_SPI1_Init();
  MX_TIM2_Init();
  MX_USART1_UART_Init();
  MX_TIM14_Init();
  /* USER CODE BEGIN 2 */
  LL_ADC_Enable(ADC1);

  snake.red = 0;
  snake.green = 150;
  snake.blue  = 200;
  snake.index_body = 0;
  snake.index_hand = 0;
  snake.hand_red = 150;
  snake.hand_green = 0;
  snake.hand_blue = 255;
  snake.index_tail = 0;
  snake.length = 5;
  snake.direction = DIR_LEFT;//DIR_RIGHT;//
  snake.coordinate_snake[0] = NUMBER_POINT_LINE/2;

  apple_point.red = 245;
  apple_point.green = 0;
  apple_point.blue  = 0;

  LL_SPI_Enable (SPI1);
  CS_OFF
  ;
  RES_OFF
  ;
  DATA
  ;

  LL_TIM_CC_EnableChannel (TIM2, LL_TIM_CHANNEL_CH4);
  LL_TIM_EnableCounter (TIM2);

  ST7789V_initial ();
  LL_GPIO_SetOutputPin (LCD_Light_GPIO_Port, LCD_Light_Pin);

  LL_TIM_DisableCounter (TIM2);

  red = 255;
  green = 255;
  blue = 255;

  print_page_color_ram(red, green, blue, SIZE_LINE, SIZE_COLUMN);

//  SPI_WriteComm (0x3C);




/*
  for(uint8_t tmp = 0; tmp < 16; tmp ++)
  {
	  print_page_color (red, green, blue, SIZE_DISPLAY);
	  red += 16;
  }

  */
//  print_page_color (255, 255, 255, SIZE_DISPLAY);


  LL_TIM_EnableIT_CC1 (TIM14);
  LL_TIM_EnableCounter (TIM14);

  LL_USART_EnableIT_RXNE (USART1);

//  Vertical_Scrolling_Definition (16, 288, 16);


//	print_picture();

  /*
   for(uint8_t tmp = 0; tmp < 20; tmp ++)
   {
   print_string(symbol, 24 );
   symbol += 24;
   }
   */

//	Convert_String(data_test);
  /*
   for(i = 0; i < 20; i ++)
   {
   data_test[i] = 128 + i;
   }
   */
/*
  Print_String (" ");
//	Print_String(data_test);
  Print_String (" ");
  Print_String ("     Привет  Мир :)  ");
  Print_String (" ");
  Print_String ("     Hello World :)  ");
  Print_String (" ");
  Print_String (" Мой Дисплей работает !");
  Print_String (" ");
  Print_String ("    My Display work ! ");
  Print_String (" ");
  Print_String (" 1234567890 ");

//  Print_Sys_time();
//  LL_USART_ReceiveData8(USART1);

  memset (Data_to_display, 0, 24);
  put_str ("\n Hello World \n");

  cnt_line_write = NUMBER_OF_LINES - 2;

  Vertical_Scrolling_Definition (16, 288, 16);

  SetXY_windows (70, 149, 2, 17);

//	SPI_WriteComm(0x2C);
//	print_page_color(255, 255, 0, (17*(241-70-70)));

//		Print_Sys_time();

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */

	LL_ADC_REG_SetSequencerChRem(ADC1, 1);
	LL_ADC_REG_StartConversion(ADC1);

	while(LL_ADC_IsActiveFlag_EOC(ADC1)==0);

	LL_ADC_ClearFlag_EOC(ADC1);
	data_ADC1 = LL_ADC_REG_ReadConversionData12(ADC1);
	adc_tmp_rand += data_ADC1;

	LL_ADC_REG_SetSequencerChRem(ADC1, 1);
	LL_ADC_REG_StartConversion(ADC1);

	generate_apple();

	while (1)
    {
		if (sys_timers[0] == 0)
		{
			snake_brange();
			if(Button[0])
			{
				switch(snake.direction)
				{
				case	DIR_LEFT:		snake.direction = DIR_DOWN; 	break;
				case	DIR_RIGHT:		snake.direction = DIR_UP; 		break;
				case	DIR_DOWN:		snake.direction = DIR_RIGHT; 	break;
				case	DIR_UP:			snake.direction = DIR_LEFT; 	break;
				}
				Button[0] = 0;
			}
			else
			{
				if(Button[2])
				{
					switch(snake.direction)
					{
					case	DIR_LEFT:		snake.direction = DIR_UP; 		break;
					case	DIR_RIGHT:		snake.direction = DIR_DOWN; 	break;
					case	DIR_DOWN:		snake.direction = DIR_LEFT; 	break;
					case	DIR_UP:			snake.direction = DIR_RIGHT; 	break;
					}
					Button[2] = 0;
				}
			}

			if((LL_GPIO_IsInputPinSet(But1_GPIO_Port, But2_Pin) == 0)||(Button[1]))
			{
				sys_timers[0] = 20;
				Button[1] = 0;
			}
			else
			{
				sys_timers[0] = 60;
			}

			print_snake_point(snake);
			snake.index_body = snake.index_hand;
			snake.index_hand ++;
			if(snake.index_hand >= MAX_LENGHT_SNAKE)
			{
				snake.index_hand = 0;
				snake.coordinate_snake[snake.index_hand] = (SIZE_MATRIX + snake.coordinate_snake[MAX_LENGHT_SNAKE-1] + snake.direction) % SIZE_MATRIX;
			}
			else
			{
				snake.coordinate_snake[snake.index_hand] = (SIZE_MATRIX + snake.coordinate_snake[snake.index_hand-1] + snake.direction) % SIZE_MATRIX;
			}

			if(((snake.index_hand + MAX_LENGHT_SNAKE - snake.index_tail) % MAX_LENGHT_SNAKE) > snake.length)
			{
				clear_snake_point(snake.index_tail);
				snake.index_tail ++;
				if(snake.index_tail >= MAX_LENGHT_SNAKE)
					snake.index_tail = 0;
				check_rules();
			}

			if(apple_point.coordinate == snake.coordinate_snake[snake.index_hand])
			{
				generate_apple();
				if(snake.length < MAX_LENGHT_SNAKE) snake.length++;
				else snake.length = 5;
				Print_Score();
			}

		}


		if(LL_ADC_IsActiveFlag_EOC(ADC1))
		{
			LL_ADC_ClearFlag_EOC(ADC1);
			data_ADC1 = LL_ADC_REG_ReadConversionData12(ADC1);
			adc_tmp_rand += data_ADC1;

			LL_ADC_REG_SetSequencerChRem(ADC1, 1);
			LL_ADC_REG_StartConversion(ADC1);

		}


/*
      LL_ADC_REG_SetSequencerChRem(ADC1, 1);
      LL_ADC_REG_StartConversion(ADC1);

      while(LL_ADC_IsActiveFlag_EOC(ADC1)==0);
      LL_ADC_ClearFlag_EOC(ADC1);
      data_ADC1 = LL_ADC_REG_ReadConversionData12(ADC1);
      data_voltege_1 = data_ADC1*2508/3116;

      if (sys_timers[0] == 0)
		{
		  sys_timers[0] = 30;
		  vsa++;
		  if (vsa >= 304)
			vsa = 16;
		  SetXY_windows (0, 240, vsa, vsa + 1);
		  SPI_WriteComm (0x2C);
		  print_page_color (255, 255, 255, SIZE_LINE); // ( red, green, blue)
		  Vertical_Scrolling_Start_Address (vsa);
		  SetXY_windows (0, 240, vsa - 1, vsa);
		  SPI_WriteComm (0x2C);
		  print_page_color (255, 255, 255, ((data_ADC1 * 240 / 4096) % 240)); // ( red, green, blue)
		  print_page_color (255, 0, 255, 1); // ( red, green, blue)
		  print_page_color (255, 255, 255,
					(240 - ((data_ADC1 * 240 / 4096) % 240)) - 1); // ( red, green, blue)

		}
		*/

      SetXY_windows (240, 319, 2, 17);
      Print_Sys_time ();

      /*
       if(flag_clear_line)
       {
       if(sys_timers[0] == 0)
       {
       sys_timers[0] = 20;
       vsa++;
       if(vsa >= 304) vsa = 16;
       SetXY_windows(0,240,vsa,vsa+1);
       SPI_WriteComm(0x2C);
       print_page_color(255, 255, 255, SIZE_LINE); // ( red, green, blue)
       Vertical_Scrolling_Start_Address(vsa);
       if(++ cnt_clear_line >= 16)
       flag_clear_line = 0;

       }
       }

       //		Print_String(" 1234567890 ");

       SetXY_windows(70,149,2,17);
       Print_Sys_time();


       if(Data_to_display[0])
       {
       memcpy(tmpdr,Data_to_display,24);
       memset(Data_to_display,0,24);

       if(cnt_line_write >= (NUMBER_OF_LINES-1))
       {
       flag_clear_line = 1;
       cnt_clear_line = 0;
       }

       SetXY_windows(0,240,(16+((cnt_line_write-1) % (NUMBER_OF_LINES))*16),(16+(((cnt_line_write-1) % (NUMBER_OF_LINES) )+1)*16));
       cnt_line_write ++;


       Print_Data_to_display(SECTOR,240,tmpdr);
       }
       */

      /*
       if((USART1->ISR & USART_ISR_RXNE))
       {
       tmpdr[0] = LL_USART_ReceiveData8(USART1);
       Print_Data_to_display(ALL_STRING,240,tmpdr);
       }
       */

      /*
       if(LL_GPIO_IsInputPinSet(But1_GPIO_Port, But1_Pin) == 0)
       {
       But1 = 1;
       }
       else But1 = 0;

       if(LL_GPIO_IsInputPinSet(But2_GPIO_Port, But2_Pin) == 0)
       {
       if(LL_USART_IsActiveFlag_TXE(USART1))
       {
       LL_USART_TransmitData8(USART1, 0x55);
       }
       But2 = 1;
       }
       else But2 = 0;

       if(LL_GPIO_IsInputPinSet(But3_GPIO_Port, But3_Pin) == 0)
       {
       LL_GPIO_SetOutputPin(LCD_Light_GPIO_Port, LCD_Light_Pin);
       GPIOF->ODR |= (1<<0);
       But3 = 1;
       }
       else But3 = 0;

    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */

    }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  LL_FLASH_SetLatency(LL_FLASH_LATENCY_1);

  if(LL_FLASH_GetLatency() != LL_FLASH_LATENCY_1)
  {
  Error_Handler();  
  }
  LL_RCC_HSI14_Enable();

   /* Wait till HSI14 is ready */
  while(LL_RCC_HSI14_IsReady() != 1)
  {
    
  }
  LL_RCC_HSI14_SetCalibTrimming(16);
  LL_RCC_HSI48_Enable();

   /* Wait till HSI48 is ready */
  while(LL_RCC_HSI48_IsReady() != 1)
  {
    
  }
  LL_RCC_SetAHBPrescaler(LL_RCC_SYSCLK_DIV_1);
  LL_RCC_SetAPB1Prescaler(LL_RCC_APB1_DIV_1);
  LL_RCC_SetSysClkSource(LL_RCC_SYS_CLKSOURCE_HSI48);

   /* Wait till System clock is ready */
  while(LL_RCC_GetSysClkSource() != LL_RCC_SYS_CLKSOURCE_STATUS_HSI48)
  {
  
  }
  LL_Init1msTick(48000000);
  LL_SYSTICK_SetClkSource(LL_SYSTICK_CLKSOURCE_HCLK);
  LL_SetSystemCoreClock(48000000);
  LL_RCC_HSI14_EnableADCControl();
  LL_RCC_SetUSARTClockSource(LL_RCC_USART1_CLKSOURCE_PCLK1);
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(char *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
