/**
  ******************************************************************************
  * File Name          : SPI.c
  * Description        : This file provides code for the configuration
  *                      of the SPI instances.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "spi.h"

/* USER CODE BEGIN 0 */
#include "main.h"
#include "stm32f0xx.h"

static const uint8_t image_data_Image[1] = { 0
};

/* USER CODE END 0 */

/* SPI1 init function */
void MX_SPI1_Init(void)
{
  LL_SPI_InitTypeDef SPI_InitStruct = {0};

  LL_GPIO_InitTypeDef GPIO_InitStruct = {0};
  /* Peripheral clock enable */
  LL_APB1_GRP2_EnableClock(LL_APB1_GRP2_PERIPH_SPI1);
  
  LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOA);
  /**SPI1 GPIO Configuration  
  PA5   ------> SPI1_SCK
  PA6   ------> SPI1_MISO
  PA7   ------> SPI1_MOSI 
  */
  GPIO_InitStruct.Pin = LL_GPIO_PIN_5;
  GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
  GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_HIGH;
  GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
  GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
  GPIO_InitStruct.Alternate = LL_GPIO_AF_0;
  LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  GPIO_InitStruct.Pin = LL_GPIO_PIN_6;
  GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
  GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_HIGH;
  GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
  GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
  GPIO_InitStruct.Alternate = LL_GPIO_AF_0;
  LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  GPIO_InitStruct.Pin = LL_GPIO_PIN_7;
  GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
  GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_HIGH;
  GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
  GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
  GPIO_InitStruct.Alternate = LL_GPIO_AF_0;
  LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  SPI_InitStruct.TransferDirection = LL_SPI_FULL_DUPLEX;
  SPI_InitStruct.Mode = LL_SPI_MODE_MASTER;
  SPI_InitStruct.DataWidth = LL_SPI_DATAWIDTH_8BIT;
  SPI_InitStruct.ClockPolarity = LL_SPI_POLARITY_LOW;
  SPI_InitStruct.ClockPhase = LL_SPI_PHASE_1EDGE;
  SPI_InitStruct.NSS = LL_SPI_NSS_SOFT;
  SPI_InitStruct.BaudRate = LL_SPI_BAUDRATEPRESCALER_DIV4;
  SPI_InitStruct.BitOrder = LL_SPI_MSB_FIRST;
  SPI_InitStruct.CRCCalculation = LL_SPI_CRCCALCULATION_DISABLE;
  SPI_InitStruct.CRCPoly = 7;
  LL_SPI_Init(SPI1, &SPI_InitStruct);
  LL_SPI_SetStandard(SPI1, LL_SPI_PROTOCOL_MOTOROLA);
  LL_SPI_DisableNSSPulseMgt(SPI1);

}

/* USER CODE BEGIN 1 */

void SPI_WriteComm(uint8_t send_comand)
{
  CS_ON;
  COMAND;
	LL_SPI_TransmitData8(SPI1,send_comand);
  //SPI1->DR = send_comand;                       // Пишем в буфер передатчика SPI3. После этого стартует обмен данными
  Delay(3);
  while((SPI1->SR & SPI_SR_BSY));               // Ожидаем окончания отправки данных модулем SPI3 (TXNE =1 - приемный буфер содержит данные)
  CS_OFF;
}

void SPI_WriteData(uint8_t send_data)
{
  CS_ON;
  DATA;
//  SPI1->DR = send_data;                       	// Пишем в буфер передатчика SPI3. После этого стартует обмен данными
	LL_SPI_TransmitData8(SPI1,send_data);
  Delay(3);
  while((SPI1->SR & SPI_SR_BSY));               // Ожидаем окончания отправки данных модулем SPI3 (TXNE =1 - приемный буфер содержит данные)
  CS_OFF;
}

void print_picture(void)
{
	CS_ON;
	DATA;

  uint8_t red;
  uint8_t green;
  uint8_t blue;
  
  for(uint32_t i=0; i<76800; i++)
  {
    red = image_data_Image[i*3];
    while((SPI1->SR & SPI_SR_TXE)==0);   				// Ожидаем окончания отправки данных модулем SPI3 (TXNE =1 - приемный буфер содержит данные)
		LL_SPI_TransmitData8(SPI1,red);
//    SPI1->DR = red;                     				// Пишем в буфер передатчика SPI3. После этого стартует обмен данными
    green = image_data_Image[(i*3)+1];
    while((SPI1->SR & SPI_SR_TXE)==0);      		// Ожидаем окончания отправки данных модулем SPI3 (TXNE =1 - приемный буфер содержит данные)
		LL_SPI_TransmitData8(SPI1,green);
//    SPI1->DR = green;                      			// Пишем в буфер передатчика SPI3. После этого стартует обмен данными
    blue = image_data_Image[(i*3)+2];
    while((SPI1->SR & SPI_SR_TXE)==0);      		// Ожидаем окончания отправки данных модулем SPI3 (TXNE =1 - приемный буфер содержит данные)
		LL_SPI_TransmitData8(SPI1,blue);
//    SPI1->DR = blue;                      			// Пишем в буфер передатчика SPI3. После этого стартует обмен данными
  }
	Delay(3);
	while(SPI1->SR & SPI_SR_BSY);
	CS_OFF;
  
}

void print_page_color_ram (uint8_t red, uint8_t green, uint8_t blue, uint32_t cnt_pX, uint32_t cnt_pY)
{
    SetXY_windows (0, cnt_pX, 0, cnt_pY);

	SPI_WriteComm(0x2C);

	CS_ON;
	DATA;
	for(uint32_t i=0; i<(cnt_pX*cnt_pY); i++)
	{
		while((SPI1->SR & SPI_SR_TXE)==0);       	// Ожидаем окончания отправки данных модулем SPI3 (TXNE =1 - приемный буфер содержит данные)
		LL_SPI_TransmitData8(SPI1,red);
		while((SPI1->SR & SPI_SR_TXE)==0);     		// Ожидаем окончания отправки данных модулем SPI3 (TXNE =1 - приемный буфер содержит данные)
		LL_SPI_TransmitData8(SPI1,green);
		while((SPI1->SR & SPI_SR_TXE)==0);      	// Ожидаем окончания отправки данных модулем SPI3 (TXNE =1 - приемный буфер содержит данные)
		LL_SPI_TransmitData8(SPI1,blue);
	}
	Delay(3);
	while(SPI1->SR & SPI_SR_BSY);
	CS_OFF;
}

void print_page_color(uint8_t red, uint8_t green, uint8_t blue, uint32_t cnt_px)
{
	CS_ON;
	DATA;
  
  for(uint32_t i=0; i<cnt_px; i++)
  {
	while((SPI1->SR & SPI_SR_TXE)==0);       	// Ожидаем окончания отправки данных модулем SPI3 (TXNE =1 - приемный буфер содержит данные)
	LL_SPI_TransmitData8(SPI1,red);
	while((SPI1->SR & SPI_SR_TXE)==0);     		// Ожидаем окончания отправки данных модулем SPI3 (TXNE =1 - приемный буфер содержит данные)
	LL_SPI_TransmitData8(SPI1,green);
	while((SPI1->SR & SPI_SR_TXE)==0);      	// Ожидаем окончания отправки данных модулем SPI3 (TXNE =1 - приемный буфер содержит данные)
	LL_SPI_TransmitData8(SPI1,blue);
  }
	Delay(3);
	while(SPI1->SR & SPI_SR_BSY);
	CS_OFF;
  
}

void print_pix_color(uint8_t red, uint8_t green, uint8_t blue)
{
	while((SPI1->SR & SPI_SR_TXE)==0);       	// Ожидаем окончания отправки данных модулем SPI3 (TXNE =1 - приемный буфер содержит данные)
	LL_SPI_TransmitData8(SPI1,red);
	while((SPI1->SR & SPI_SR_TXE)==0);     		// Ожидаем окончания отправки данных модулем SPI3 (TXNE =1 - приемный буфер содержит данные)
	LL_SPI_TransmitData8(SPI1,green);
	while((SPI1->SR & SPI_SR_TXE)==0);      	// Ожидаем окончания отправки данных модулем SPI3 (TXNE =1 - приемный буфер содержит данные)
	LL_SPI_TransmitData8(SPI1,blue);
}
void SetXY_windows (uint16_t XS, uint16_t XE, uint16_t YS, uint16_t YE)
{
	SPI_WriteComm(0x2A);											// Column address set 
	DATA;
	CS_ON;
	LL_SPI_TransmitData8(SPI1,(XS>>8));
	while((SPI1->SR & SPI_SR_TXE)==0);       	// Ожидаем окончания отправки данных модулем SPI1 (TXNE =1 - приемный буфер содержит данные)
	LL_SPI_TransmitData8(SPI1,(XS&0xFF));
	while((SPI1->SR & SPI_SR_TXE)==0);     		// Ожидаем окончания отправки данных модулем SPI1 (TXNE =1 - приемный буфер содержит данные)
	LL_SPI_TransmitData8(SPI1,(XE>>8));
	while((SPI1->SR & SPI_SR_TXE)==0);      	// Ожидаем окончания отправки данных модулем SPI1 (TXNE =1 - приемный буфер содержит данные)
	LL_SPI_TransmitData8(SPI1,(XE&0xFF));
	Delay(3);
	while(SPI1->SR & SPI_SR_BSY);
	
	SPI_WriteComm(0x2B);											// Row address set 
	DATA;
	CS_ON;
	LL_SPI_TransmitData8(SPI1,(YS>>8));
	while((SPI1->SR & SPI_SR_TXE)==0);       	// Ожидаем окончания отправки данных модулем SPI1 (TXNE =1 - приемный буфер содержит данные)
	LL_SPI_TransmitData8(SPI1,(YS&0xFF));
	while((SPI1->SR & SPI_SR_TXE)==0);     		// Ожидаем окончания отправки данных модулем SPI1 (TXNE =1 - приемный буфер содержит данные)
	LL_SPI_TransmitData8(SPI1,(YE>>8));
	while((SPI1->SR & SPI_SR_TXE)==0);      	// Ожидаем окончания отправки данных модулем SPI1 (TXNE =1 - приемный буфер содержит данные)
	LL_SPI_TransmitData8(SPI1,(YE&0xFF));

	Delay(3);
	while(SPI1->SR & SPI_SR_BSY);
	CS_OFF;
}

void Vertical_Scrolling_Definition (uint16_t TFA, uint16_t VSA, uint16_t BFA)
{
	SPI_WriteComm(0x33);											// Vertical scrolling definition
  DATA;
  CS_ON;
	LL_SPI_TransmitData8(SPI1,(TFA>>8));
	while((SPI1->SR & SPI_SR_TXE)==0);       	// Ожидаем окончания отправки данных модулем SPI1 (TXNE =1 - приемный буфер содержит данные)
	LL_SPI_TransmitData8(SPI1,(TFA&0xFF));
	while((SPI1->SR & SPI_SR_TXE)==0);     		// Ожидаем окончания отправки данных модулем SPI1 (TXNE =1 - приемный буфер содержит данные)
	LL_SPI_TransmitData8(SPI1,(VSA>>8));
	while((SPI1->SR & SPI_SR_TXE)==0);      	// Ожидаем окончания отправки данных модулем SPI1 (TXNE =1 - приемный буфер содержит данные)
	LL_SPI_TransmitData8(SPI1,(VSA&0xFF));
	while((SPI1->SR & SPI_SR_TXE)==0);      	// Ожидаем окончания отправки данных модулем SPI1 (TXNE =1 - приемный буфер содержит данные)
	LL_SPI_TransmitData8(SPI1,(BFA>>8));
	while((SPI1->SR & SPI_SR_TXE)==0);       	// Ожидаем окончания отправки данных модулем SPI1 (TXNE =1 - приемный буфер содержит данные)
	LL_SPI_TransmitData8(SPI1,(BFA&0xFF));

	Delay(3);
	while(SPI1->SR & SPI_SR_BSY);
	CS_OFF;
}

void Vertical_Scrolling_Start_Address (uint16_t VSA)
{
	SPI_WriteComm(0x37);											// Vertical scrolling definition
  DATA;
  CS_ON;
	LL_SPI_TransmitData8(SPI1,(VSA>>8));
	while((SPI1->SR & SPI_SR_TXE)==0);       	// Ожидаем окончания отправки данных модулем SPI1 (TXNE =1 - приемный буфер содержит данные)
	LL_SPI_TransmitData8(SPI1,(VSA&0xFF));

	Delay(3);
	while(SPI1->SR & SPI_SR_BSY);
	CS_OFF;
}


/* USER CODE END 1 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
